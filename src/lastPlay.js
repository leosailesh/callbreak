import { Suit } from "./card.js";
import {
  getRemaining,
  groupCardsBySuit,
  getHighestPlayedCard,
} from "./utils.js";

export const lastPlay = (
  players,
  playerId,
  history,
  cards,
  allCards,
  played
) => {
  const { S, C, D, H } = groupCardsBySuit(cards);
  const currentSuit = played[0].suit;
  const largestPlayedCard = getHighestPlayedCard(played);
  const suitOfCardAvailable = cards[0].suit;
  if (largestPlayedCard.suit === currentSuit) {
    if (currentSuit === suitOfCardAvailable) {
      return { value: cards[cards.length - 1].toString() };
    } else if (suitOfCardAvailable === Suit.SPADE) {
      return { value: cards[cards.length - 1].toString() };
    } else {
      // play card of least value
      const allCardsExceptSpades = [...H, ...C, ...D];
      if (allCardsExceptSpades.length) {
        const lowestValueCardInHand = allCardsExceptSpades.sort(
          (a, b) => a.rank.value - b.rank.value
        )[0];
        return { value: lowestValueCardInHand.toString() };
      }
    }
  }
  return { value: cards[cards.length - 1].toString() };
};
