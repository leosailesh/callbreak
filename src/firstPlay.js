import { Rank, Suit } from "./card.js";
import {
  checkSpadeCut,
  getCurrentTrumpOfSuit,
  getRemaining,
  groupCardsBySuit,
} from "./utils.js";
import { mcts } from "./mcts.js";

export const firstPlay = (
  players,
  playerId,
  history,
  cards,
  playerIds,
  timeBudget,
  payload
) => {
  // if already got points, play spade from largest if available
  const bidAmount = players[playerId].bid;
  const wonAmount = players[playerId].won;
  const remaining = getRemaining(cards, [], history);
  const {
    S: remainingSpades,
    H: remainingHearts,
    C: remainingClubs,
    D: remainingDiamonds,
  } = groupCardsBySuit(remaining);
  const { S, C, D, H } = groupCardsBySuit(cards);

  if (S.length) {
    const trump = getCurrentTrumpOfSuit(remainingSpades, S);
    if (trump) {
      return { value: trump.toString() };
    }
  }
  if (!remainingSpades.length) {
    if (H.length) {
      const trump = getCurrentTrumpOfSuit(remainingHearts, H);
      if (trump) {
        return { value: trump.toString() };
      }
    }

    if (C.length) {
      const trump = getCurrentTrumpOfSuit(remainingClubs, C);
      if (trump) {
        return { value: trump.toString() };
      }
    }

    if (D.length) {
      const trump = getCurrentTrumpOfSuit(remainingDiamonds, D);
      if (trump) {
        return { value: trump.toString() };
      }
    }
  }

  if (H.length) {
    const { isSpadeCut, hasMoreSpades, winnerIdx } = checkSpadeCut(
      history,
      Suit.HEART.code
    );
    if (!(isSpadeCut && hasMoreSpades)) {
      let ace = H.find((card) => card.rank === Rank.ACE);
      if (ace && remainingHearts.length > 5) {
        return { value: ace.toString() };
      }
      if (H.length && H[0].rank === Rank.KING) {
        if (remainingHearts.length) {
          if (remainingHearts[0].rank === Rank.ACE) {
            if (H.length > 1) {
              return { value: H[1].toString() };
            }
          } else {
            // check if we have a spade
            if (S.length && bidAmount > wonAmount) {
              return { value: S[S.length - 1].toString() };
            }
            return { value: H[0].toString() };
          }
        }
      }
    } else if (isSpadeCut) {
      if (!hasMoreSpades) {
        const trump = getCurrentTrumpOfSuit(remainingHearts, H);
        if (trump) {
          return { value: trump.toString() };
        }
      }
    }
  }
  // check spade cut for suit
  if (C.length) {
    const { isSpadeCut, hasMoreSpades, winnerIdx } = checkSpadeCut(
      history,
      Suit.CLUB.code
    );
    if (!(isSpadeCut && hasMoreSpades)) {
      let ace = C.find((card) => card.rank === Rank.ACE);
      if (ace && remainingClubs.length > 5) {
        return { value: ace.toString() };
      }
      if (C.length && C[0].rank === Rank.KING) {
        if (remainingClubs.length) {
          if (remainingClubs[0].rank === Rank.ACE) {
            if (C.length > 1) {
              return { value: C[1].toString() };
            }
          } else {
            // check if we have a spade
            if (S.length && bidAmount > wonAmount) {
              return { value: S[S.length - 1].toString() };
            }
            return { value: C[0].toString() };
          }
        }
      }
    } else if (isSpadeCut) {
      if (!hasMoreSpades) {
        const trump = getCurrentTrumpOfSuit(remainingClubs, C);
        if (trump) {
          return { value: trump.toString() };
        }
      }
    }
  }

  if (D.length) {
    // check spade cut for suit
    const { isSpadeCut, hasMoreSpades, winnerIdx } = checkSpadeCut(
      history,
      Suit.DIAMOND.code
    );
    if (!(isSpadeCut && hasMoreSpades)) {
      let ace = D.find((card) => card.rank === Rank.ACE);
      if (ace && remainingDiamonds.length > 5) {
        return { value: ace.toString() };
      }
      if (D.length && D[0].rank === Rank.KING) {
        if (remainingDiamonds.length) {
          if (remainingDiamonds[0].rank === Rank.ACE) {
            if (D.length > 1) {
              return { value: D[1].toString() };
            }
          } else {
            // check if we have a spade
            if (S.length && bidAmount > wonAmount) {
              return { value: S[S.length - 1].toString() };
            }
            return { value: D[0].toString() };
          }
        }
      }
    } else if (isSpadeCut) {
      if (!hasMoreSpades) {
        const trump = getCurrentTrumpOfSuit(remainingDiamonds, D);
        if (trump) {
          return { value: trump.toString() };
        }
      }
    }
  }
  if (wonAmount >= bidAmount) {
    if (S.length) {
      return { value: S[S.length - 1].toString() };
    }
  }
  // if (wonAmount < bidAmount) {
  //   // todo use mcts to get best move
  //   const bestMove = mcts(payload);
  //   return bestMove;
  // }

  // if (S.length && S[0].rank === Rank.KING) {
  //   if (remainingSpades.length) {
  //     if (remainingSpades[0].rank === Rank.ACE) {
  //       if (S.length > 1) {
  //         return { value: S[1].toString() };
  //       }
  //     } else {
  //       return { value: S[0].toString() };
  //     }
  //   }
  // }

  // todo add condition for following cases which to go with
  if (S.length > remainingSpades.length / 3) {
    // throw lowest count card
    if (H.length < C.length && H.length < D.length && H.length) {
      return { value: H[H.length - 1].toString() };
    }
    if (C.length < H.length && C.length < D.length && C.length) {
      return { value: C[C.length - 1].toString() };
    }
    if (D.length < H.length && D.length < C.length && D.length) {
      return { value: D[D.length - 1].toString() };
    }
  }
  if (S.length > 0 && S.length < remainingSpades.length / 3) {
    // throw highest count card
    if (H.length > C.length && H.length > D.length) {
      return { value: H[H.length - 1].toString() };
    }
    if (C.length > H.length && C.length > D.length) {
      return { value: C[C.length - 1].toString() };
    }
    if (D.length > H.length && D.length > C.length) {
      return { value: D[D.length - 1].toString() };
    }
  }
  // throw lowest rank card
  const allCardsExceptSpades = [...H, ...C, ...D];
  if (allCardsExceptSpades.length) {
    const lowestValueCardInHand = allCardsExceptSpades.sort(
      (a, b) => a.rank.value - b.rank.value
    )[0];
    return { value: lowestValueCardInHand.toString() };
  }

  return { value: cards[cards.length - 1].toString() };
};
