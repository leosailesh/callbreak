import { groupCardsBySuit, parseCardArr } from "./utils.js";
import { serialTrumpSpadeCheck, serialTrumpCheck } from "./bidHelpers.js";

export function bid(payload) {
  /*
      Bid is called at the starting phase of the game in callbreak.
      You will be provided with the following data:
      {
          "timeBudget": -4615,
          "id": "Bot-1",
          "cards": [
              "QS",
              "5S",
              "1H",
              "KH",
              "QH",
              "9H",
              "7H",
              "5H",
              "4H",
              "KC",
              "5C",
              "6D",
              "3D"
          ],
          "context": {
              "round": 2,
              "players": {
                  "P0": {
                      "totalPoints": 2.2,
                      "bid": 3
                  },
                  "Bot-1": {
                      "totalPoints": 2.1,
                      "bid": 0
                  },
                  "Bot-2": {
                      "totalPoints": 3,
                      "bid": 3
                  },
                  "Bot-3": {
                      "totalPoints": 2.1,
                      "bid": 1
                  }
              }
          }
      }
      This is all the data that you will require for the bidding phase.
      */

  /*
     ####################################
     #     Input your code here.        #
     ####################################
     */

  // reset global cases for next round
  const {
    cards: cardsStrArr,
    context: { round, players },
    playerId,
    playerIds,
  } = payload;
  let maxBid = 8;
  let minBid = 1;
  let totalBid = 0;
  let bidderCount = 0;
  Object.values(players).forEach((player) => {
    if (player.bid > 0) {
      totalBid += player.bid;
      bidderCount++;
    }
  });
  if (bidderCount === 3) {
    maxBid = 13 - totalBid;
    maxBid = maxBid > 8 ? 8 : maxBid;
    minBid = 8 - totalBid;
    minBid = minBid < 1 ? 1 : minBid;
    if (round === 5) {
      // todo if last one to bid and last round, set maxBid according to min required to win
    }
  }
  if (round === 5) {
    // todo if last one to bid and last round, set maxBid according to min required to win
    let maxScoreOfOthers = 0;
    playerIds.map((each) => {
      if (each === playerId) {
        return;
      }
      maxScoreOfOthers = Math.max(maxScoreOfOthers, players[each].totalPoints);
    });
    maxScoreOfOthers = Math.ceil(maxScoreOfOthers);
    const possibleHighScore = maxScoreOfOthers + 4;
    const myCurrentScore = Math.floor(players[playerId].totalPoints);
    maxBid = possibleHighScore - myCurrentScore;
  }
  const cards = parseCardArr(cardsStrArr);
  const { S, C, D, H } = groupCardsBySuit(cards);
  let count = 0;
  count = serialTrumpSpadeCheck(S);
  let spadeLeftCount = S.length - count - 1;
  // console.log({ count, spadeLeftCount });
  count = serialTrumpCheck(count, C, S.length);
  count = serialTrumpCheck(count, D, S.length);
  count = serialTrumpCheck(count, H, S.length);
  // let countMissing = 0;
  // if (C.length < 3) {
  //   const diff = 3 - C.length;
  //   if (diff > 0) {
  //     const spadesUsed = Math.min(spadeLeftCount, diff);
  //     count += spadesUsed;
  //     spadeLeftCount -= spadesUsed;
  //     countMissing += 1;
  //   }
  // }
  // if (H.length < 3) {
  //   const diff = 3 - H.length;
  //   if (diff > 0) {
  //     const spadesUsed = Math.min(spadeLeftCount, diff);
  //     count += spadesUsed;
  //     spadeLeftCount -= spadesUsed;
  //     countMissing += 1;
  //   }
  // }

  // if (D.length < 3) {
  //   const diff = 3 - D.length;
  //   if (diff > 0) {
  //     const spadesUsed = Math.min(spadeLeftCount, diff);
  //     count += spadesUsed;
  //     spadeLeftCount -= spadesUsed;
  //     countMissing += 1;
  //   }
  // }
  // if (countMissing > 1) {
  //   count -= 1;
  // }
  if (spadeLeftCount > 1) {
    count += spadeLeftCount - 2;
  } else if (spadeLeftCount === 1) {
    // // give a point for a missing card set
    // let countMissing = 0;
    // if (C.length < 3) {
    //   const diff = 3 - C.length;
    //   if (diff > 0) {
    //     const spadesUsed = Math.min(spadeLeftCount, diff);
    //     count += spadesUsed;
    //     spadeLeftCount -= spadesUsed;
    //     countMissing += 1;
    //   }
    // }
    // if (H.length < 3) {
    //   const diff = 3 - H.length;
    //   if (diff > 0) {
    //     const spadesUsed = Math.min(spadeLeftCount, diff);
    //     count += spadesUsed;
    //     spadeLeftCount -= spadesUsed;
    //     countMissing += 1;
    //   }
    // }
    // if (D.length < 3) {
    //   const diff = 3 - D.length;
    //   if (diff > 0) {
    //     const spadesUsed = Math.min(spadeLeftCount, diff);
    //     count += spadesUsed;
    //     spadeLeftCount -= spadesUsed;
    //     countMissing += 1;
    //   }
    // }
    // if (countMissing > 1) {
    //   count -= 1;
    // }
  }

  // 8 is max allowed bid
  if (count !== 8 || round === 5) {
    count = count > maxBid ? maxBid : count;
  }
  count = count < 1 ? 1 : count;
  // 1 is minimum bid
  return { value: Math.max(minBid, count) };
}
