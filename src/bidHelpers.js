import { Rank, Suit } from "./card.js";
import { parseCardArr } from "./utils.js";

export const serialTrumpSpadeCheck = (spades) => {
  let count = 0;
  let minWinBySpade = 0;

  const spadesInHandArr = spades.map((card) => card.toString());

  const allSpades = [
    "1S",
    "KS",
    "QS",
    "JS",
    "TS",
    "9S",
    "8S",
    "7S",
    "6S",
    "5S",
    "4S",
    "3S",
    "2S",
  ];
  const allRemainingSpadesArr = allSpades.filter(
    (each) => !spadesInHandArr.includes(each)
  );
  let opponentRemainingSpadeCount = Math.ceil(allRemainingSpadesArr.length / 3);
  if (opponentRemainingSpadeCount === 2 && spades.length === 7) {
    opponentRemainingSpadeCount = 3;
  }
  // console.log({ opponentRemainingSpadeCount });
  const filterTrump = spades.filter((each, index) => {
    if (allSpades[index] === each.toString()) {
      minWinBySpade += 1;
      opponentRemainingSpadeCount -= 1;
      return false;
    }
    return true;
  });
  if (minWinBySpade >= 3) {
    return spades.length;
  }
  const remainingSpades = parseCardArr(allRemainingSpadesArr);
  for (let i = 0; i < filterTrump.length; i++) {
    if (opponentRemainingSpadeCount === 0) {
      minWinBySpade += 1;
      continue;
    }
    opponentRemainingSpadeCount -= 1;
    const card = filterTrump[i];
    if (i < remainingSpades.length) {
      const nextCard = remainingSpades[i];
      if (card.rank.value > nextCard.rank.value) {
        minWinBySpade += 1;
      }
    } else {
      minWinBySpade += 1;
    }
  }
  const colorCardCount = spades.length; // will be atleast 1 in valid game

  if (minWinBySpade === 0) {
    if (
      spades[0].rank === Rank.KING &&
      colorCardCount > 1 &&
      spades[1].rank.value >= 7
    ) {
      minWinBySpade += 1;
    }
  } else if (minWinBySpade === 1) {
    // if (
    //   spades[0].rank === Rank.ACE &&
    //   colorCardCount > 2 &&
    //   spades[1].rank.value >= 12
    // ) {
    //   minWinBySpade += 1;
    // }
  }
  return minWinBySpade;
};

export const serialTrumpCheck = (currentCount, cards, spadesCount) => {
  let count = currentCount;
  const colorCardCount = cards.length;

  if (colorCardCount > 0 && colorCardCount <= 6) {
    if (cards[0].rank === Rank.ACE) {
      count += 1;
      if (
        colorCardCount > 1 &&
        cards[1].rank === Rank.KING &&
        colorCardCount < 5
      ) {
        count += 1;
        if (
          colorCardCount > 2 &&
          colorCardCount < 4 &&
          cards[2].rank === Rank.QUEEN
        ) {
          count += 1;
        }
      }
    } else if (
      cards[0].rank === Rank.KING &&
      colorCardCount > 1 &&
      colorCardCount <= 5
    ) {
      if (spadesCount > 2 || colorCardCount <= 4) {
        count += 1;
      }
    }
  }
  return count;
};
