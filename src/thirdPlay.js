import { Suit } from "./card.js";
import {
  getRemaining,
  groupCardsBySuit,
  getHighestPlayedCard,
} from "./utils.js";
import { mcts } from "./mcts.js";

export const thirdPlay = (
  players,
  playerId,
  history,
  cards,
  allCards,
  played,
  playerIds,
  payload
) => {
  const bidAmount = players[playerId].bid;
  const wonAmount = players[playerId].won;

  const allRemaining = getRemaining(allCards, [], history);
  const currentSuit = played[0].suit;
  const largestPlayedCard = getHighestPlayedCard(played);
  const suitOfCardAvailable = cards[0].suit;
  if (largestPlayedCard.suit === currentSuit) {
    if (currentSuit === suitOfCardAvailable) {
      const remainingCardsOfSameSuit = allRemaining.filter(
        (each) => each.suit === currentSuit
      );
      if (!remainingCardsOfSameSuit.length) {
        return { value: cards[cards.length - 1].toString() };
      } else {
        // check if cards[0] is largest from remaining
        const remainingBiggerCards = remainingCardsOfSameSuit.filter(
          (each) => each.rank.value > cards[0].rank.value
        );
        if (remainingBiggerCards.length) {
          if (remainingBiggerCards[0].rank.value < cards[0].rank.value + 3) {
            return { value: cards[0].toString() };
          }
          return { value: cards[cards.length - 1].toString() };
        }
        return { value: cards[0].toString() };
      }
    } else if (suitOfCardAvailable === Suit.SPADE) {
      return { value: cards[cards.length - 1].toString() };
    } else {
      // play card of least value
      const { C, D, H } = groupCardsBySuit(cards);
      const allCardsExceptSpades = [...H, ...C, ...D];
      if (allCardsExceptSpades.length) {
        const lowestValueCardInHand = allCardsExceptSpades.sort(
          (a, b) => a.rank.value - b.rank.value
        )[0];
        return { value: lowestValueCardInHand.toString() };
      }
      return { value: cards[cards.length - 1].toString() };
    }
  } else if (suitOfCardAvailable === Suit.SPADE) {
    return { value: cards[cards.length - 1].toString() };
  } else {
    // if (wonAmount < bidAmount) {
    //   // todo use mcts to get best move
    //   const bestMove = mcts(payload);
    //   return bestMove;
    // }

    const { C, D, H } = groupCardsBySuit(cards);
    // play card of least value
    const allCardsExceptSpades = [...H, ...C, ...D];
    if (allCardsExceptSpades.length) {
      const lowestValueCardInHand = allCardsExceptSpades.sort(
        (a, b) => a.rank.value - b.rank.value
      )[0];
      return { value: lowestValueCardInHand.toString() };
    }
  }
  return { value: cards[cards.length - 1].toString() };
};
