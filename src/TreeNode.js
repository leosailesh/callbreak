import { playGame, isGameComplete } from "./GamePlay.js";
import {
  getHighestPlayedCard,
  getRemaining,
  parseCardArr,
  ruleFilter,
  shuffle,
} from "./utils.js";

const c = Math.sqrt(2);

export class TreeNode {
  constructor(payload, gameState, parent) {
    this.payload = payload;

    const played = parseCardArr(gameState.played);
    if (this.payload === null) {
      this.gameState = gameState;
      // randomly set other player cards in gameState for rest simulations
      const allRemaining = getRemaining(
        parseCardArr(this.gameState.cards),
        played,
        this.gameState.history
      );
      this.gameState.allPlayerCards = [[], [], [], []];
      const allRemainingStr = allRemaining.map((each) => each.toString());
      shuffle(allRemainingStr);
      const myPlayerIdx = this.gameState.playerIds.indexOf(
        this.gameState.playerId
      );
      this.gameState.myPlayerIdx = myPlayerIdx;
      this.gameState.allPlayerCards[myPlayerIdx] = this.gameState.cards;
      if (this.gameState.history.length) {
        // check history if any player has missing cards

        const firstPlayerIndex =
          this.gameState.history[this.gameState.history.length - 1][2];
        const tricsCompleted = this.gameState.history.length;
        const cardsRemaining = 13 - tricsCompleted;
        let playedLength = this.gameState.played.length;
        for (let i = 0; i < 4; i += 1) {
          const currentPlayerIndex = (firstPlayerIndex + i) % 4;
          if (currentPlayerIndex === myPlayerIdx) {
            continue;
          }
          // todo check history if player has some missing suit to make simulations more accurate
          if (playedLength) {
            playedLength -= 1;
            this.gameState.allPlayerCards[currentPlayerIndex] =
              allRemainingStr.splice(0, cardsRemaining - 1);
            continue;
          }
          this.gameState.allPlayerCards[currentPlayerIndex] =
            allRemainingStr.splice(0, cardsRemaining);
        }
      } else {
        // todo in case no history, set all player cards to all remaining cards
        let playedLength = this.gameState.played.length;
        const cardsRemaining = 13;
        const firstPlayerIndex = (myPlayerIdx - played.length + 4) % 4;
        for (let i = 0; i < 4; i += 1) {
          const currentPlayerIndex = (firstPlayerIndex + i) % 4;
          if (currentPlayerIndex === myPlayerIdx) {
            continue;
          }
          // todo check history if player has some missing suit to make simulations more accurate
          if (playedLength) {
            playedLength -= 1;
            this.gameState.allPlayerCards[currentPlayerIndex] =
              allRemainingStr.splice(0, cardsRemaining - 1);
            continue;
          }
          this.gameState.allPlayerCards[currentPlayerIndex] =
            allRemainingStr.splice(0, cardsRemaining);
        }
      }
    } else {
      this.gameState = {
        ...gameState,
        played: [...gameState.played, payload.value],
        allPlayerCards: gameState.allPlayerCards.map((each) =>
          each.filter((card) => card !== payload.value)
        ),
      };
      if (this.gameState.played.length === 4) {
        // add to history
        let startingPlayerIndex = 0;
        if (this.gameState.history.length) {
          startingPlayerIndex =
            this.gameState.history[this.gameState.history.length - 1][2];
        } else {
          const myPlayerIdx = this.gameState.playerIds.indexOf(
            this.gameState.playerId
          );
          startingPlayerIndex =
            (myPlayerIdx - this.gameState.played.length + 4) % 4;
        }
        let winningPlayerIndex = 0;
        const largestPlayedCard = getHighestPlayedCard(played).toString();
        const largestPlayedIndex =
          this.gameState.played.indexOf(largestPlayedCard);
        winningPlayerIndex = (startingPlayerIndex + largestPlayedIndex) % 4;
        this.gameState.history.push([
          startingPlayerIndex,
          this.gameState.played,
          winningPlayerIndex,
        ]);
        const winningPlayer = this.gameState.playerIds[winningPlayerIndex];
        this.gameState.context.players[winningPlayer].won += 1;
        this.gameState.played = [];
        this.gameState.playerId = winningPlayer;
      } else {
        const currentPlayerIndex = this.gameState.playerIds.indexOf(
          this.gameState.playerId
        );
        this.gameState.playerId =
          this.gameState.playerIds[(currentPlayerIndex + 1) % 4];
      }
    }
    this.children = [];
    this.parent = parent;
    this.win = 0;
    this.n = 0;

    [this.terminated, this.terminationValue] = isGameComplete(this.gameState);
  }

  getUCB(T) {
    if (this.n === 0) return Number.POSITIVE_INFINITY;

    const k = this.win / this.n + c * Math.sqrt(Math.log2(T) / this.n);
    return k;
  }

  search(T) {
    if (this.children.length === 0) return this;

    // Select the first child node
    let selectedNode = this.children[0];
    let selectedUCB = selectedNode.getUCB(T);

    for (let i = 1; i < this.children.length; i += 1) {
      const n = this.children[i];
      const ucb = n.getUCB(T);

      if (ucb > selectedUCB) {
        selectedNode = n;
        selectedUCB = ucb;
      }
    }

    return selectedNode.search(T);
  }

  expand() {
    if (this.terminated) return this;

    let result = this;
    // find valid cards to play
    let validCards = [];
    const currentPlayerIndex = this.gameState.playerIds.indexOf(
      this.gameState.playerId
    );
    if (this.gameState.played.length === 0) {
      validCards = this.gameState.allPlayerCards[currentPlayerIndex];
    } else {
      const allCards = parseCardArr(
        this.gameState.allPlayerCards[currentPlayerIndex]
      );
      const played = parseCardArr(this.gameState.played);
      validCards = ruleFilter(played, allCards).map((each) => each.toString());
    }

    for (let i = 0; i < validCards.length; i += 1) {
      const childNode = new TreeNode(
        { value: validCards[i] },
        this.gameState,
        this
      );
      if (!childNode.terminated) {
        result = childNode;
      }
      this.children.push(childNode);
    }

    return result;
  }

  simulate() {
    if (this.terminated) {
      this.backPropogate(this.terminationValue);
    } else {
      const w = playGame(this.gameState);
      this.backPropogate(w);
    }
  }

  backPropogate(w) {
    this.win += w;
    this.n += 1;

    if (this.parent) {
      this.parent.backPropogate(1 - w);
    }
  }
}
