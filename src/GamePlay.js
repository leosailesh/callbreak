import { parseCardArr, ruleFilter, getHighestPlayedCard } from "./utils.js";

function convertState(state) {
  // console.log({ state });
  const {
    played: playedStrArr,
    history,
    playerId,
    context: { players },
    playerIds,
    timeBudget,
    myPlayerIdx,
    allPlayerCards,
  } = state;
  let played = history.length;
  const myPlayerId = playerIds[myPlayerIdx];
  let bid = players[myPlayerId].bid;
  let won = players[myPlayerId].won;
  return {
    played,
    playedStrArr,
    myPlayerIdx,
    playerId,
    playerIds,
    allPlayerCards,
    won,
    bid,
  };
}

export function isGameComplete(state) {
  const { played, won, bid } = convertState(state);
  // if all cards played, game is complete
  if (played === 13) {
    // if my score is equal to bid
    if (won === bid) {
      return [true, 1];
    }
    // if my score is greater than bid
    if (won > bid) {
      return [true, 0.5];
    }
    // if my score is less than bid
    if (won < bid) {
      return [true, 0];
    }
  }
  return [false, 0];
}

export function playGame(state) {
  let {
    played,
    playedStrArr,
    playerId,
    playerIds,
    allPlayerCards,
    won,
    bid,
    myPlayerIdx,
  } = convertState(state);
  let currentPlayerIndex = playerIds.indexOf(playerId);
  let startingPlayerIndex = (currentPlayerIndex - playedStrArr.length + 4) % 4;
  while (allPlayerCards.flat().length > 0) {
    // const currentPlayerIndex
    while (playedStrArr.length !== 4) {
      currentPlayerIndex = playerIds.indexOf(playerId);
      if (playedStrArr.length === 0) {
        const k = Math.floor(
          Math.random() * allPlayerCards[currentPlayerIndex].length
        );
        const currentCard = allPlayerCards[currentPlayerIndex].splice(k, 1)[0];
        playedStrArr.push(currentCard);
        playerId = playerIds[(currentPlayerIndex + 1) % 4];
      } else {
        const availableCards = ruleFilter(
          parseCardArr(playedStrArr),
          parseCardArr(allPlayerCards[currentPlayerIndex])
        );
        const k = Math.floor(Math.random() * availableCards.length);
        const currentCard = availableCards[k].toString();
        playedStrArr.push(currentCard);
        const currentCardIndex =
          allPlayerCards[currentPlayerIndex].indexOf(currentCard);
        allPlayerCards[currentPlayerIndex].splice(currentCardIndex, 1)[0];
        playerId = playerIds[(currentPlayerIndex + 1) % 4];
      }
    }
    // find winner
    const largestPlayedCard = getHighestPlayedCard(
      parseCardArr(playedStrArr)
    ).toString();
    const largestPlayedIndex = playedStrArr.indexOf(largestPlayedCard);
    startingPlayerIndex = (startingPlayerIndex + largestPlayedIndex) % 4; // winner will start next round
    if (startingPlayerIndex === myPlayerIdx) {
      won += 1;
    }
    playedStrArr = [];
    playerId = playerIds[startingPlayerIndex];
  }
  if (won === bid) {
    return 1;
  }
  // if my score is greater than bid
  if (won > bid) {
    return 0.5;
  }
  // if my score is less than bid
  if (won < bid) {
    return 0;
  }
}
