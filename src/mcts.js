import { TreeNode } from "./TreeNode.js";

const simulations = 500;

export function mcts(state) {
  const rootNode = new TreeNode(null, state, null);
  rootNode.expand();

  // for (let i = 0; i < simulations; i += 1) {
  const toTime = new Date();
  let i = 0;
  while (new Date() - toTime < 135) {
    const selectedNode = rootNode.search(i++).expand();
    selectedNode.simulate();
  }

  let selectedNode = null;
  rootNode.children.forEach((node) => {
    // console.log("Win = ", node.win, "Simul", node.n);
    if (selectedNode === null || node.n > selectedNode.n) {
      selectedNode = node;
    }
  });

  // console.log({ selectedNode });
  return selectedNode.payload;
}
