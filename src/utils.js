import { Card, Suit } from "./card.js";

export const cardValues = [
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "T",
  "J",
  "Q",
  "K",
  "1",
];

export const reverseCardValues = [
  "1",
  "K",
  "Q",
  "J",
  "T",
  "9",
  "8",
  "7",
  "6",
  "5",
  "4",
  "3",
  "2",
];

export const fullDeck = ["H", "D", "C", "S"]
  .map((suit) => reverseCardValues.map((value) => `${value}${suit}`))
  .flat();
export const fullDeckCards = fullDeck.map((card) => new Card(card));

function isCard1StrongerThanCard2(card1, card2, currentColor) {
  const card1Value = card1.charAt(0);
  const card1Color = card1.charAt(1);
  const card2Value = card2.charAt(0);
  const card2Color = card2.charAt(1);
  if (currentColor === "S" && card1Color !== "S") {
    return false;
  }
  if (card1Color === "S" && card2Color !== "S") {
    return true;
  }
  if (card1Color !== "S" && card2Color === "S") {
    return false;
  }
  if (card1Color !== currentColor && card1Color !== "S") {
    return false;
  }
  if (card1Color === card2Color) {
    return cardValues.indexOf(card1Value) > cardValues.indexOf(card2Value);
  }
  return true;
}

export function getLargestPlayed(played = []) {
  if (played.length === 1) {
    return played[0];
  }
  const firstCardColor = played[0].charAt(1);
  const result = played.reduce((acc, curr) => {
    if (isCard1StrongerThanCard2(curr, acc, firstCardColor)) {
      return curr;
    } else {
      return acc;
    }
  }, played[0]);
  return result;
}

export function isCard1StrongerThanCards(card1, cards, currentColor) {
  let result = false;
  let weakerCards = [];
  let strongerCards = [];
  cards.map((each) => {
    if (isCard1StrongerThanCard2(card1, each, currentColor)) {
      weakerCards.push(each);
    } else {
      strongerCards.push(each);
    }
  });
  if (strongerCards.length === 0) {
    result = true;
  }
  return { result, weakerCards, strongerCards };
}

export function getCurrentTrumpOfSuit(remainingSuitCards, suitCardsInHand) {
  if (!suitCardsInHand.length) {
    return null;
  }
  const remainingSuitCardsInOrder = remainingSuitCards.sort(
    (a, b) => b.rank.value - a.rank.value
  );
  const suitCardsInOrder = suitCardsInHand.sort(
    (a, b) => b.rank.value - a.rank.value
  );
  if (!remainingSuitCards.length) {
    return suitCardsInOrder[suitCardsInOrder.length - 1];
  } else if (
    suitCardsInOrder[0].rank.value > remainingSuitCardsInOrder[0].rank.value
  ) {
    return suitCardsInOrder[0];
  }
  return null;
}
export function groupCardsBySuit(cards = []) {
  const S = cards
    .filter((card) => card.suit === Suit.SPADE)
    .sort((a, b) => b.rank.value - a.rank.value);
  const C = cards
    .filter((card) => card.suit === Suit.CLUB)
    .sort((a, b) => b.rank.value - a.rank.value);
  const H = cards
    .filter((card) => card.suit === Suit.HEART)
    .sort((a, b) => b.rank.value - a.rank.value);
  const D = cards
    .filter((card) => card.suit === Suit.DIAMOND)
    .sort((a, b) => b.rank.value - a.rank.value);
  return { S, C, H, D };
}

export function checkSpadeCut(history, currentSuit) {
  for (let i = history.length - 1; i >= 0; i--) {
    const [startIdx, play, winnerIdx] = history[i];
    if (!play[0].includes(currentSuit)) {
      continue;
    }
    if (
      play[1].includes(currentSuit) &&
      play[2].includes(currentSuit) &&
      play[3].includes(currentSuit)
    ) {
      return { isSpadeCut: false, hasMoreSpades: false };
    } else if (play.find((each) => each.includes("S"))) {
      return { isSpadeCut: true, hasMoreSpades: true, winnerIdx };
    } else {
      return { isSpadeCut: true, hasMoreSpades: false, winnerIdx };
    }
  }
  return { isSpadeCut: false, hasMoreSpades: false, winnerIdx: null };
}

export function filterSmallerCardsUpto(cards = [], upto = "6") {
  return cards.filter(
    (each) => cardValues.indexOf(each.charAt(0)) > cardValues.indexOf(upto)
  );
}

export function filterLargerCardsFrom(cards = [], from = "6") {
  return cards
    .filter(
      (each) => cardValues.indexOf(each.charAt(0)) <= cardValues.indexOf(from)
    )
    .reverse();
}

export function parseCardArr(cards = []) {
  return cards.map((card) => new Card(card));
}

export function sameSuit(card1, card2) {
  return card1.suit.value === card2.suit.value;
}

export function getRemaining(cards = [], played = [], history = []) {
  const sanitizeHistory = history
    .map((each) => {
      const [startIdx, play, winnerIdx] = each;
      return parseCardArr(play);
    })
    .flat();
  const allKnownCards = [...cards, ...played, ...sanitizeHistory];
  let remaining = [...fullDeckCards];
  allKnownCards.map((each) => {
    remaining = remaining.filter(
      (card) => !(card.rank === each.rank && card.suit === each.suit)
    );
  });
  return remaining;
}

/**
 * @param played Cards that have been played this hand
 * @returns The highest rank card that has been played of the same suit as the first played card.
 * If card of another rank has been played then that is not returned.
 */
export function getHighestPlayedCard(played) {
  let highestCard = played[0];
  for (let i = 1; i < played.length; i++) {
    const card = played[i];
    if (sameSuit(card, highestCard)) {
      if (card.rank.value > highestCard.rank.value) {
        highestCard = card;
      }
    } else if (card.suit === Suit.SPADE) {
      highestCard = card;
    }
  }
  return highestCard;
}

export function ruleFilter(played, cardsInHand) {
  let result = [...cardsInHand];
  // rule 1;
  const sameSuitCards = cardsInHand.filter(
    (each) => each.suit === played[0].suit
  );
  if (sameSuitCards.length) {
    result = [...sameSuitCards];
    // rule 2:
    const largestPlayed = getHighestPlayedCard(played);
    if (largestPlayed.suit === played[0].suit) {
      const largerCards = result.filter(
        (each) => each.rank.value > largestPlayed.rank.value
      );
      if (largerCards.length) {
        result = [...largerCards];
      }
    }
  } else if (played[0].suit !== Suit.SPADE) {
    // rule 2:
    const largestPlayed = getHighestPlayedCard(played);
    const spadesCards = cardsInHand.filter((each) => each.suit === Suit.SPADE);
    if (largestPlayed.suit === Suit.SPADE) {
      const largerCards = spadesCards.filter(
        (each) => each.rank.value > largestPlayed.rank.value
      );
      if (largerCards.length) {
        result = [...largerCards];
      }
    } else {
      if (spadesCards.length) {
        result = [...spadesCards];
      }
    }
  }
  // filter sequence cards
  const newResult = result.reduce(
    (acc, each) => {
      if (acc.final.length) {
        const lastCard = acc.prev;
        if (
          lastCard.suit.value === each.suit.value &&
          lastCard.rank.value === each.rank.value + 1
        ) {
          return { final: acc.final, prev: each };
        }
      }
      return { final: [...acc.final, each], prev: each };
    },
    { final: [], prev: null }
  );
  return newResult.final;
}

export function shuffle(array) {
  // Fisher-Yates algorithm
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1)); // random index from 0 to i
    [array[i], array[j]] = [array[j], array[i]];
  }
}
