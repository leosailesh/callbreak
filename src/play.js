import { getHighestPlayedCard, parseCardArr, ruleFilter } from "./utils.js";
import { firstPlay } from "./firstPlay.js";
import { secondPlay } from "./secondPlay.js";
import { thirdPlay } from "./thirdPlay.js";
import { lastPlay } from "./lastPlay.js";

export function play(payload) {
  const {
    cards: allCardsInHand,
    played: playedStrArr,
    history,
    playerId,
    context: { players },
    playerIds,
    timeBudget,
  } = payload;

  // console.log(mcts(payload));
  // return {};
  if (allCardsInHand.length === 1) {
    return { value: allCardsInHand[0] };
  }
  const allCards = parseCardArr(allCardsInHand);
  const played = parseCardArr(playedStrArr);

  if (!played.length) {
    const firstPlayValue = firstPlay(
      players,
      playerId,
      history,
      allCards,
      playerIds,
      timeBudget,
      payload
    );
    return firstPlayValue;
  }
  const cards = ruleFilter(played, allCards);
  if (cards.length === 1) {
    return { value: cards[0].toString() };
  }
  const currentSuit = played[0].suit;
  const suitOfCardAvailable = cards[0].suit;
  if (currentSuit === suitOfCardAvailable) {
    if (cards[0].rank.value < played[0].rank.value) {
      return { value: cards[cards.length - 1].toString() };
    }
    const largestPlayedCard = getHighestPlayedCard(played);
    if (largestPlayedCard.suit === currentSuit) {
      if (cards[0].rank.value < largestPlayedCard.rank.value) {
        return { value: cards[cards.length - 1].toString() };
      }
    } else {
      return { value: cards[cards.length - 1].toString() };
    }
  }

  if (played.length === 1) {
    const secondPlayValue = secondPlay(
      players,
      playerId,
      history,
      cards,
      allCards,
      played,
      playerIds,
      payload
    );
    return secondPlayValue;
  }
  if (played.length === 2) {
    const thirdPlayValue = thirdPlay(
      players,
      playerId,
      history,
      cards,
      allCards,
      played,
      playerIds,
      payload
    );
    return thirdPlayValue;
  }
  if (played.length === 3) {
    const lastPlayValue = lastPlay(
      players,
      playerId,
      history,
      cards,
      allCards,
      played,
      playerIds
    );
    return lastPlayValue;
  }
}
