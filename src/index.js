import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import { play } from "./play.js";
import { bid } from "./bid.js";

const app = express();

app.use(cors());

app.use(bodyParser.json());

app.get("/hi", (req, res) => {
  res.json({ value: "hello" });
});

app.post("/play", (req, res) => {
  const payload = req.body;
  const response = play(payload);
  res.json(response);
});

app.post("/bid", (req, res) => {
  const payload = req.body;
  const response = bid(payload);
  res.json(response);
});

app.listen(7000);

// const testPayload = {
//   timeBudget: 1435,
//   playerId: "P0",
//   playerIds: ["P0", "Bot-1", "Bot-2", "Bot-3"],
//   cards: ["6S", "9H", "4H", "3H", "QC", "9C", "5C", "2C"],
//   played: ["6C", "8C"],
//   history: [
//     [2, ["5D", "KD", "4D", "2D"], 3],
//     [3, ["4S", "7S", "KS", "2S"], 1],
//     [1, ["7D", "9D", "1D", "3D"], 3],
//     [3, ["9S", "JS", "5H", "QS"], 2],
//     [2, ["JD", "TD", "6D", "8D"], 2],
//   ],
//   context: {
//     round: 6,
//     players: {
//       P0: { totalPoints: 6.0, bid: 1, won: 0 },
//       "Bot-1": { totalPoints: 6.5, bid: 2, won: 0 },
//       "Bot-2": { totalPoints: 8.4, bid: 3, won: 0 },
//       "Bot-3": { totalPoints: -2.0, bid: 3, won: 0 },
//     },
//   },
// };
// const bidPayload = {
//   timeBudget: 1500,
//   playerId: "P0",
//   playerIds: ["P0", "Bot-1", "Bot-2", "Bot-3"],
//   cards: [
//     "1S",
//     "QS",
//     "3S",
//     "TH",
//     "4H",
//     "2H",
//     "KC",
//     "JC",
//     "8C",
//     "6C",
//     "6D",
//     "4D",
//     "2D",
//   ],
//   context: {
//     round: 1,
//     players: {
//       P0: { totalPoints: 0, bid: 0, won: 0 },
//       "Bot-1": { totalPoints: 0, bid: 0, won: 0 },
//       "Bot-2": { totalPoints: 0, bid: 0, won: 0 },
//       "Bot-3": { totalPoints: 0, bid: 0, won: 0 },
//     },
//   },
// };

// console.log(bid(bidPayload));
// console.log(play(testPayload));
