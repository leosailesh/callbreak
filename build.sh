#!/bin/bash

docker build --platform linux/amd64 -t callbreak .
docker save callbreak | gzip > callbreak.tar.gz