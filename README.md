docker run -d -p 127.0.0.1:7000:7000 callbreak
docker run -p 127.0.0.1:7000:7000 -m=2048m --cpus=1 callbreak

callbreak rules:

1. if same color card available cannot put other color cards
2. if higher cards than highest played available cannot put smaller cards

`play` method should do the following:

- [x] throw early if only one card in hand
- [x] throw early if only one possible card in hand
- [x] make possible for King to win in next round
- [x] if not first player filter cards in hand with rule 1 and rule 2.
- [x] finish lowest count card first
- play and win 8 card wins

- keep track of spade cutters also if their spade is over
- keep track of color play (2+ means it can be cut by spade)
- if point is already got play reckelessly. finish spade. give point to spade cutters. etc

`bid` method should do the following:

- [x] spade check first
- [x] add 1 value for Ace of each suite.
- [x] add 1 value for K of each suite.
- [x] add 1 value for Q if both A & K present of same suit.
- [x] if any suite except spade has less than 3 add value for missing count by available spades
- [] consider last round and bid safe if winning

PT = Probability Table

Table 1. Side suit high card’s value. Calculated as the probability that the first/ second/ third trick can not be cut by an opponent, taking into account the player’s number of cards from that suit.

| cards in side-suit | Probability that | both      | opponents have: |
| ------------------ | ---------------- | --------- | --------------- |
|                    | > 0 cards        | > 1 cards | > 2 cards       |
| 0                  | 0.997            | 0.966     | 0.817           |
| 1                  | 0.994            | 0.942     | 0.733           |
| 2                  | 0.990            | 0.907     | 0.624           |
| 3                  | 0.983            | 0.855     | 0.489           |
| 4                  | 0.970            | 0.779     | 0.350           |
| 5                  | 0.948            | 0.678     | 0.212           |
| 6                  | 0.915            | 0.544     | 0.095           |
| 7                  | 0.857            | 0.381     | 0.025           |
| 8                  | 0.774            | 0.214     | 0               |
| 9                  | 0.646            | 0.074     | 0               |
| 10                 | 0.462            | 0         | 0               |
| 11                 | 0.227            | 0         | 0               |

1 Input: Hand, PrevBids; Result: bid [1-8]
2 regularTakes ← CalcRegularTakes(PT,Hand,PrevBids);
3 nilValue ← CalcNilValue(PT,Hand);
4 nilProb ← SC(PrevBids,nilValue);
5 expNilScore ← nilProb ·100 + (1-nilProb) (-100);
6 nilThreshold← CalcNilThreshold(regularTakes);
7 if expNilScore > nilThreshold then
8 | Return 0;
9 else
10 |Return regularTakes;
11 end

1. Regular bid:
   In a regular bid, BIS tries to estimate the largest number of tricks it can take with high probability. Five features are considered to calculate the regular bid of a hand: (1) side suits high cards (2) spades high cards, (3) long spades suit, (4) short side suits accompanied with spades and (5) the sum of the previous bids. Features 2,3 and 4 are not completely disjoint: the value of a spade card is the maximum between the value it gets from high/long spades and the value it gets from being a potential cut at a short side-suits.

1.1. Side Suit High Cards
